# Changelog

## v3.5.9

* Flash fla -> xfl converted
* Flash fixed fonts in UI
* work with 1.13.0

## v3.5.8

* repo moved to gitlab